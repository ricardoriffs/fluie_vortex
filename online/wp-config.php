<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'equilate_wp800');

/** MySQL database username */
define('DB_USER', 'equilate_wp800');

/** MySQL database password */
define('DB_PASSWORD', '@6apfD8.5S');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
	

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eo25rdkl61ydtev21bsbh2mkhb2sz0uxfaznsspv2d1nuhyurgrnhszmzp3r5tcs');
define('SECURE_AUTH_KEY',  'puiz5tq36dljlxcrhrmgiola9nhdziwc0qxpxwscyknkmisv2f3nqyryv4o1pyze');
define('LOGGED_IN_KEY',    'voabkud84rspwuh18mygdunumpcq0n1nq4rk3tpfmy8sw6iurpx9dkbahpayoqbe');
define('NONCE_KEY',        'o9qolik6b1pzmpei1wht9h3vrrrpy4invzmalq37dsuuegskztktfobnsdwfwccr');
define('AUTH_SALT',        'qgckaxzppfxftmoukznaao5jgrpiwjkoajkvfelfrxiskehzq0copwrld1dbfm5l');
define('SECURE_AUTH_SALT', 'f2fxwtva3frz4anvox6dabzolsdqzaycvyh3jrxecyrmvjk7qm9mb6gouzjvql90');
define('LOGGED_IN_SALT',   'ezw8yukyq0vehkdpypnj2akowxkf6rve5rxlw4vnypojy5e9q78jwqmqq7thsuty');
define('NONCE_SALT',       'rbooex7oeeyar8zi1c9jwcij8jxy5frnb5255i2nnc38nuphc4u4udaelk1s8ayj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp7k_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
