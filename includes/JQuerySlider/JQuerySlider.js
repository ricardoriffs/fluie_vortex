var JQuerySlider_init = {
	"align":"left",
   "auto":true,
   "hideOnMouseOut":true,
   "vertical":false,
   "ease":"linear",
   "continuous":true,
   "loop":true,
   "sliderEffect":"slide",
   "speed":"1000",
   "fadeDuration":"1000",
   "pause":"1000",
   "stopAtInteraction":"false",
   "resumeDuration":"3000",
   "innernavbaritem.spacing":"0px",
   "innernumbers.spacing":"0px",
	
   "innerbutton.back.opacity":1,
   "innerbutton.back.hideOnMouseOut":false,
   "innerbutton.back.mouseOutEffect":"fadeIn",
   "innerbutton.back.mouseOutEffectDuration":"1000",
   "innerbutton.back.mouseOutEffectEasing":"linear",
   
   
   "innerbutton.back.halign":"left",
   "innerbutton.back.valign":"middle",
   "innerbutton.back.left":"10px",
   "innerbutton.back.top":"0px",
   
   "innerbutton.next.opacity":1,
   "innerbutton.next.hideOnMouseOut":false,
   "innerbutton.next.mouseOutEffect":"fadeIn",
   "innerbutton.next.mouseOutEffectDuration":"1000",
   "innerbutton.next.mouseOutEffectEasing":"linear",
   
   
   "innerbutton.next.halign":"left",
   "innerbutton.next.valign":"top",
   "innerbutton.next.left":"0px",
   "innerbutton.next.top":"0px",
   
   "innerplaypause.opacity":1,
   "innerplaypause.hideOnMouseOut":false,
   "innerplaypause.mouseOutEffect":"fadeIn",
   "innerplaypause.mouseOutEffectDuration":"1000",
   "innerplaypause.mouseOutEffectEasing":"linear",
	 
   "innerplaypause.halign":"left",
   "innerplaypause.valign":"top",
   "innerplaypause.left":"0px",
   "innerplaypause.top":"0px",
   
   
   "innernavbar.hideOnMouseOut":false,
   "innernavbar.mouseOutEffect":"fadeIn",
   "innernavbar.mouseOutEffectDuration":"1000",
   "innernavbar.mouseOutEffectEasing":"linear",
   
   "innernavbar.halign":"left",
   "innernavbar.valign":"top",
   "innernavbar.left":"0px",
   "innernavbar.top":"0px",
   "innernavbaritem.orientation":"horizontal",
 
   "innerbullets.valign":"middle",
   "innerbullets.halign":"left",
   "innerbullets.left":"40px",
   "innerbullets.hideOnMouseOut":false,
   "innerbullets.mouseOutEffect":"fadeIn",
   "innerbullets.mouseOutEffectEasing":"linear",
   "innerbullets.mouseOutEffectDuration":"1000",
   "innerbulletsitem.orientation":"horizontal",
   
   "innernumbers.valign":"middle",
   "innernumbers.halign":"left",
   "innernumbers.left":"40px",
   "innernumbers.hideOnMouseOut":false,
   "innernumbers.mouseOutEffect":"fadeIn",
   "innernumbers.mouseOutEffectEasing":"linear",
   "innernumbers.mouseOutEffectDuration":"1000",
   "innernumbersitem.orientation":"horizontal",
   
   "content_paragraph.opacity": "1",
   "content_subheader.opacity": "1",
   "content_header.opacity": "1"
};

(function(c){c.fn.sudoSlider=function(h){var l=!0,d=!l,h=c.extend({prevNext:l,prevHtml:'<a href="#" class="prevBtn"> previous </a>',nextHtml:'<a href="#" class="nextBtn"> next </a>',controlsShow:l,controlsAttr:'id="controls"',controlsFadeSpeed:"400",controlsFade:l,insertAfter:l,firstShow:d,firstHtml:'<a href="#" class="firstBtn"> first </a>',lastShow:d,lastHtml:'<a href="#" class="lastBtn"> last </a>',numericAttr:'class="controls"',numericText:["1"],vertical:d,speed:"800",ease:"swing",auto:d,pause:"2000",
continuous:d,clickableAni:d,numeric:d,updateBefore:d,history:d,speedhistory:"400",autoheight:l,customLink:d,fade:d,crossFade:l,fadespeed:"1000",ajax:d,loadingText:"Loading Content...",preloadAjax:d,startSlide:d,ajaxLoadFunction:d,beforeAniFunc:d,afterAniFunc:d,uncurrentFunc:d,currentFunc:d,autowidth:l,slideCount:1,resumePause:d},h),b=[h.controlsShow,h.controlsFadeSpeed,h.controlsFade,h.insertAfter,h.firstShow,h.lastShow,h.vertical,h.speed,h.ease,h.auto,h.pause,h.continuous,h.prevNext,h.numeric,h.numericAttr,
h.numericText,h.clickableAni,h.history,h.speedhistory,h.autoheight,h.customLink,h.fade,h.crossFade,h.fadespeed,h.updateBefore,h.ajax,h.preloadAjax,h.startSlide,h.ajaxLoadFunction,h.beforeAniFunc,h.afterAniFunc,h.uncurrentFunc,h.currentFunc,h.prevHtml,h.nextHtml,h.loadingText,h.firstHtml,h.controlsAttr,h.lastHtml,h.autowidth,h.slideCount,h.resumePause],u=this;return this.each(function(){function h(z,C){R=d;aa=l;w=z.children("ul");o=t=w.children("li");p=t.length;if(b[25]&&(w.length==0&&(z.append("<ul></ul>"),
w=z.children("ul")),b[25].length>p)){for(var q=1;q<=b[25].length-p;q++)w.append("<li><p>"+b[35]+"</p></li>");t=w.children("li");p=t.length}J=e=0;r=p-1;A=l;ba=E=ga=d;fa=[];R=ha=d;ia=screen.fontSmoothingEnabled;z.css("overflow","hidden");z.css("position")=="static"&&z.css("position","relative");t.css({"float":"left",display:"block"});for(q=0;q<p;q++)b[15][q]==void 0&&(b[15][q]=q+1),b[25]&&b[25][q]==void 0&&(b[25][q]=d);if(b[11]&&!b[21]){a=0;if(b[6])for(q=b[40];q>=1;q--){var f=t.eq(-b[40]+q-1).clone();
a+=f.outerWidth(l);f.css("margin-top","-"+a+"px");w.prepend(f);w.append(t.eq(b[40]-q).clone().css({"z-index":"0"}))}if(!b[6])for(q=b[40];q>=1;q--)f=t.eq(-b[40]+q-1).clone(),a+=f.outerWidth(l),f.css("margin-left","-"+a+"px"),w.prepend(f),w.append(t.eq(b[40]-q).clone().css({"z-index":"0"}));o=w.children("li");b[25]&&(b[25][0]&&F(0,b[27]==1?l:d,0,d),b[25][0]=d,b[25][r]&&F(r,b[27]==r?l:d,0,d),b[25][r]=d,b[40]>1&&j(l))}g();o=w.children("li");S=d;if(b[0]){S=c("<span "+b[37]+"></span>");b[3]?c(z).after(S):
c(z).before(S);if(b[13]){ha=S.prepend("<ol "+b[14]+"></ol>").children();f=b[11]?1:b[40];for(q=0;q<p-f+1;q++)fa[q]=c(document.createElement("li")).attr({rel:q+1}).html('<a href="#"><span>'+b[15][q]+"</span></a>").appendTo(ha).click(function(){k(c(this).attr("rel")-1,l);return d})}b[4]&&Y(b[36],"first");b[5]&&Y(b[38],"last");b[12]&&(Y(b[34],"next"),Y(b[33],"prev"))}b[26]&&j(l);b[1]=v(b[1]);b[7]=v(b[7]);b[10]=v(b[10]);b[18]=v(b[18]);b[23]=v(b[23]);b[40]=parseInt(b[40],10);b[20]&&c(b[20]).live("click",
function(){if(a=c(this).attr("rel"))a=="stop"?(b[9]=d,clearTimeout(T),ca=d):a=="start"?(b[11]||u.setOption("continuous",l),T=D(b[10]),b[9]=l):a=="block"?A=d:a=="unblock"?A=l:A&&k(a==parseInt(a)?a-1:a,l);return d});b[9]&&(T=D(b[10]));C?G(C,d,d,d):b[17]?c.address.init(function(b){C||G(m(b.value),d,d,d)}).change(function(b){b=m(b.value);b!=e&&k(b,d)}):b[27]?G(b[27]-1,d,d,d):G(0,d,d,d)}function j(c){for(var C=0;C<=r;C++)b[25][C]&&(F(C,b[27]==C&&c?l:d,0,d),b[25][C]=d)}function g(){a=0;if(b[6]){for(var c=
-1;c<=p;c++)a+=t.eq(c).outerHeight(l);w.height(a*4)}else{for(c=-1;c<=p;c++)a+=t.eq(c).outerWidth(l);w.width(a*4)}}function D(b){ca=l;return setTimeout(function(){k("next",d)},b)}function v(b){if(parseInt(b))var c=parseInt(b);else switch(c=400,b){case "fast":case "normal":case "medium":case "slow":c=600}return c}function Y(b,e){return c(b).prependTo(S).click(function(){k(e,l);return d})}function k(c,C){if(!R){if(b[9]){var q=b[7];E&&b[22]?q=parseInt(q*0.6):E&&(q=0);C?(clearTimeout(T),ca=d,b[41]&&(T=
D(q+b[41]))):T=D(b[10]+q)}b[21]?U(c,C):(b[11]&&(a=e,a<0&&(a+=p),a>r&&(a-=p),c==a+1&&(c="next"),c==a-1&&(c="prev"),a==0&&c==r&&(c="prev"),a==r&&c==0&&(c="next")),G(c,C,l,d))}}function f(b,d){b.each(function(){var b=this.tagName.toLowerCase()==="img"?c(this):c("img",this),z=this,e=0,o=function(){typeof d==="function"&&d(z)};b.length?b.each(function(){var z=this,d=function(d){z.complete||z.readyState=="complete"&&d.type=="readystatechange"?++e===b.length&&o():z.readyState===void 0&&c(z).attr("src",c(z).attr("src"))};
c(z).bind("load readystatechange",function(b){d(b)});d({type:"readystatechange"})}):o()})}function n(b){var c=b.length,e=b.substr(c-4,4);if(e==".jpg"||e==".png"||e==".bmp"||e==".gif"||b.substr(c-5,5)==".jpeg")return l;return d}function s(c){for(var c=parseInt(c>r?c=0:c<0?c=r+c+1:c)+1,d=0;d<fa.length;d++)x(fa[d],c);b[20]&&x(b[20],c)}function x(d,e){c(d).filter(".current").removeClass("current").each(function(){c.isFunction(b[31])&&b[31].call(this,c(this).attr("rel"))});c(d).filter(function(){return c(this).attr("rel")==
e}).addClass("current").each(function(){c.isFunction(b[32])&&b[32].call(this,e)})}function m(c){for(var d=0,e=0;e<=p;e+=1)b[15][e]==c&&(d=e);return d}function H(c,d){b[19]&&O(c,d);b[39]&&B(c,d)}function O(b,d){P.ready(function(){b==p&&(b=0);var e=t.eq(b).height();e!=0&&V(e,d);f(t.eq(b),function(b){P.ready(function(){e=c(b).height();e!=0&&V(e,d)})})})}function B(b,d){P.ready(function(){b==p&&(b=0);var e=t.eq(b).width();e!=0&&y(e,d);f(t.eq(b),function(b){P.ready(function(){e=c(b).width();e!=0&&y(e,
d)})})})}function y(c,e){P.animate({width:c},{queue:d,duration:e,easing:b[8]})}function V(c,e){P.animate({height:c},{queue:d,duration:e,easing:b[8]})}function Q(){b[6]?w.css("margin-top",L(e,d)):w.css("margin-left",L(e,l))}function L(c,e){for(var d=0,p=c-(b[11]&&!b[21]?1-b[40]:1),f=b[11]&&!b[21]?f=b[40]:0,j=0;j<=p;j++)d-=e?o.eq(j+f).outerWidth(l):o.eq(j+f).outerHeight(l);return d}function W(){b[11]&&!b[21]?(e>r&&(e=0),e<1-b[40]&&(e=p-b[40])):(e>r&&(e=0),e<0&&(e=r));e==r&&!b[21]&&j(d);b[24]||s(e);
Q();A=l;if(b[17]&&ga)window.location.hash=b[15][e];E||(a=e+1,a<1&&(a+=p),a>p&&(a-=p),K(t.eq(e),a),b[11]&&!b[21]&&(e<b[40]&&K(o.eq(e<0?e+b[40]:e-b[40]),a),e>r-b[40]&&K(o.eq(b[40]+e-r-1),a)))}function K(e,d){c.isFunction(b[30])&&b[30].call(e,d)}function M(e,d){c.isFunction(b[29])&&b[29].call(e,d)}function N(c,d){var o=e;switch(c){case "next":o=d>=r?b[11]?o+1:e==0?1:0:o+1;break;case "prev":o=o<=0?b[11]?o-1:e==r?r-1:r:o-1;break;case "first":o=0;break;case "last":o=r;break;default:o=parseInt(c)}return o}
function F(e,o,p,j){var g=t.eq(e),r=E?!b[22]?parseInt(b[23]*0.4):b[23]:p,h=e+1;n(b[25][e])?(g.html(" ").append(c(new Image).attr("src",b[25][e])),f(g,function(d){function p(f,g){g++;var q=f.width(),k=f.height(),m=f.parent().width();q==0&&g!=20?setTimeout(function(){p(f,g)},1):(f.attr({oldheight:k,oldwidth:q}),q>m&&f.animate({width:m,height:k/q*m},0).parent().animate({height:k/q*m},0).css("height","auto"),c.isFunction(b[28])&&b[28].call(c(d),h,l),X(e,f.parent(),j),o&&H(e,r))}var f=c(d).children();
p(f,0)})):g.load(b[25][e],function(f,p,g){if(p=="error"||!c(this).html())c(this).html("Sorry but there was an error: "+(g.status||"no content")+" "+g.statusText);X(e,c(this),j);o&&Q();p!="error"&&c.isFunction(b[28])&&b[28].call(c(this),h,d);o&&H(e,r)})}function X(e,d,f){c.isFunction(f)&&f();g();b[11]&&!b[21]&&(e<b[40]&&o.eq(e<0?e+b[40]:e-b[40]).replaceWith(c(d).clone()),e>r-b[40]&&o.eq(b[40]+e-r-1).replaceWith(c(d).clone()),o=w.children("li"))}function U(o,f){if(o!=e&&!R&&A){ba=d;b[24]&&s(N(o,J));
A=!f;var p=!f&&!b[9]&&b[17]?b[23]*(b[18]/b[7]):b[23],g=N(o,J);g>r&&(g=0);g<0&&(g=r);if(b[25]&&b[25][g])F(g,d,p,function(){b[25][g]=d;A=l;Q();U(g,l)});else{H(g,b[23]);var j=t.eq(g);M(j,g+1);if(b[22])j.clone().prependTo(P).css({"z-index":"100000",position:"absolute","list-style":"none",top:"0",left:"0"}).hide().fadeIn(b[23],function(){ia&&this.style.removeAttribute("filter");E=A=l;G(g,d,d,d);c(this).remove();if(b[17]&&f)window.location.hash=b[15][e];E=d;K(j,g+1)});else{var h=parseInt(p*0.6);p-=h;var k=
t.eq(g).children();da||(da=0);t.eq(da).children().stop();t.eq(da).children().fadeTo(p,0,function(){E=A=l;G(g,d,d,d);A=!f;k.stop().fadeTo(0,0).fadeTo(h,1,function(){ia&&this.style.removeAttribute("filter");if(b[17]&&f)window.location.hash=b[15][e];A=l;E=d;da=g;K(j,g+1)})})}}}}function G(c,g,f,j){if(A&&!R&&(N(c,J)!=e||aa)||j){ba=d;A=!g&&!b[9]?l:b[16];ga=g;J=e;e=N(c,J);b[24]&&s(e);var h=Math.sqrt(Math.abs(J-e)),k=parseInt(h*b[7]);!g&&!b[9]&&(k=parseInt(h*b[18]));f||(k=0);h=e;h<0&&(h+=p);h>r&&(h-=p);
if(j)k=ja,ea&&ea--;else if(b[25]){b[25][h]&&(F(h,l,k,d),b[25][h]=d,ba=l);if(!E){var j=J>e?e:J,m=J>e?J:e;ea=0;ja=k;for(var n=j;n<=m;n++)n<=r&&n>=0&&b[25][n]&&(F(n,d,k,function(){G(c,g,f,n)}),b[25][n]=d,ea++)}h+1<=r&&b[25][h+1]&&(F(h+1,d,0,d),b[25][h+1]=d)}if(!ea){e<0&&o.eq(b[40]+e);e>r&&o.eq(e-p-b[40]);if(!E&&(M(t.eq(h),h+1),b[11]&&!b[21]&&(e<b[40]&&M(o.eq(e<0?e+b[40]:e-b[40]),h+1),e>r-b[40]||e==-b[40])))M(o.eq(e==-b[40]?-1:b[40]+e-r-1),h+1);!E&&!ba&&H(e,k);b[6]?(h=L(e,d),w.animate({marginTop:h},{queue:d,
duration:k,easing:b[8],complete:W})):(h=L(e,l),w.animate({marginLeft:h},{queue:d,duration:k,easing:b[8],complete:W}));b[2]&&E&&parseInt(b[23]*0.6);aa=d}}}function Z(b){for(var c=["controlsShow","controlsFadeSpeed","controlsFade","insertAfter","firstShow","lastShow","vertical","speed","ease","auto","pause","continuous","prevNext","numeric","numericAttr","numericText","clickableAni","history","speedhistory","autoheight","customLink","fade","crossFade","fadespeed","updateBefore","ajax","preloadAjax",
"startSlide","ajaxLoadFunction","beforeAniFunc","afterAniFunc","uncurrentFunc","currentFunc","prevHtml","nextHtml","loadingText","firstHtml","controlsAttr","lastHtml","autowidth","slideCount","resumePause"],e=0;e<c.length;e++)if(c[e]==b)var d=e;return d}var aa,w,t,o,p,e,J,r,A,ga,E,ba,fa,ha,R,ia,S,T,$,ja,ea,ca,P=c(this),da;h(P,d);u.getOption=function(c){return b[Z(c)]};u.setOption=function(c,e){u.destroy();b[Z(c)]=e;u.init()};u.insertSlide=function(c,e,d){if(c){u.destroy();e>p&&(e=p);c="<li>"+c+"</li>";
!e||e==0?w.prepend(c):t.eq(e-1).after(c);(e<=$||!e||e==0)&&$++;if(b[15].length<e)b[15].length=e;b[15].splice(e,0,d||parseInt(e,10)+1);u.init()}};u.removeSlide=function(c){c--;u.destroy();t.eq(c).remove();b[15].splice(c,1);c<$&&$--;u.init()};u.goToSlide=function(b){k(b==parseInt(b)?b-1:b,l)};u.block=function(){A=d};u.unblock=function(){A=l};u.startAuto=function(){b[11]||u.setOption("continuous",l);b[9]=l;T=D(b[10])};u.stopAuto=function(){b[9]=d;clearTimeout(T);ca=d};u.destroy=function(){$=e;S&&S.remove();
R=l;c(b[20]).die("click");if(b[11]&&!b[21])for(a=1;a<=b[40];a++)o.eq(a-1).remove(),o.eq(-a).remove()};u.init=function(){R&&h(P,$)};u.adjust=function(b){b||(b=0);g();H(i,b)};u.getValue=function(b){switch(b){case "currentSlide":return e+1;case "totalSlides":return p;case "clickable":return A;case "destroyed":return R;case "autoAnimation":return ca}}})}})(xtd_jQuery);
(function(c){c.fn.jQuerySlider=function(){function h(b,c,e){var d={},g={},f=e.data("xtd:initial");switch(b){case "fadeIn":d.opacity=f.opacity;g.opacity=0;break;case "slideFromLeft":f.right=="auto"?(d.left=f.left,g.left=-(e.outerWidth(!0)+V+K)+"px"):(d.right=f.right,g.right=c.innerWidth()+V+K+"px");break;case "slideFromRight":f.right=="auto"?(d.left=f.left,g.left=c.innerWidth()+Q+M+"px"):(d.right=f.right,g.right=-(e.outerWidth(!0)+Q+M)+"px");break;case "slideFromTop":f.bottom=="auto"?(d.top=f.top,
g.top=-(e.outerHeight(!0)+W+F)+"px"):(d.bottom=f.bottom,g.bottom=c.innerHeight()+W+F+"px");break;case "slideFromBottom":f.bottom=="auto"?(d.top=f.top,g.top=c.innerHeight()+L+N+"px"):(d.bottom=f.bottom,g.bottom=-(e.outerHeight(!0)+L+N)+"px")}return{"in":d,out:g}}function l(b,c,e){b.data("xtd:initial",{left:b.css("left"),right:b.css("right"),top:b.css("top"),bottom:b.css("bottom"),opacity:e});var d=h(g[c+".mouseOutEffect"],k,b),f=g[c+".mouseOutEffectDuration"],j=g[c+".mouseOutEffectEasing"];b.animate(d.out,
0);B.mouseenter(function(b){return function(){b.animate(d["in"],parseInt(f),j)}}(b)).mouseleave(function(b){return function(){b.animate(d.out,parseInt(f),j)}}(b))}function d(){I.browser.msie?(t.children().filter(".play").css({display:"none"}),t.children().filter(".pause").css({display:"block"})):(c("div[id^='"+j+"_playpause']").children().filter(".play").css({display:"none"}),c("div[id^='"+j+"_playpause']").children().filter(".pause").css({display:"block"}))}function b(){I.browser.msie?(t.children().filter(".play").css({display:"block"}),
t.children().filter(".pause").css({display:"none"})):(c("div[id^='"+j+"_playpause']").children().filter(".play").css({display:"block"}),c("div[id^='"+j+"_playpause']").children().filter(".pause").css({display:"none"}))}function u(b){var d=0;b.children().each(function(){d+=c(this).outerWidth(!0)});return d}var I=c,j=c(this).attr("id");I.globalEval("var global = this;");var g=global[j+"_json"],g=I.extend({},JQuerySlider_init,g);g.autoheight=!1;g.autowidth=!1;g.clickableAni=!0;g.updateBefore=!0;g.clickableAni=
!1;g.controlsShow=!1;g.numeric=!1;g.hideOnMouseOut=!0;g.crossFade=!1;g.fadeDuration=parseInt(g.fadeDuration);g.speed=parseInt(g.speed);g.resumePause=g.stopAtInteraction?!1:parseInt(g.resumeDuration);g.pause=parseInt(g.pause);g.fadespeed=g.speed;g.fade=g.sliderEffect=="fade"?!0:!1;var D=["innernavbar","innerbutton.back","innerbutton.next","innerplaypause"],v=[["navbar","bullets","numbers"],["back"],["next"],["playpause"]],Y=[["navbar","bullets","numbers"]],k=c("#"+j+"Container");k.children().children("ul").children("li").children("div").children("a:only-child").each(function(){c(this).parent().click(function(b){b.preventDefault()});
var b=c(this),d=c(this).parent();d.parent().click(function(){window.open(b.attr("href"),b.attr("target"))});d.parent().mouseenter(function(){c(this).css("cursor","pointer")}).mouseleave(function(){c(this).css("cursor","default")})});for(var f=0;f<v[0].length;f++){var n=g["inner"+v[0][f]+".itemsalign"],s=c("div[id='"+j+"_"+v[0][f]+"_in'] ul");if(s.length){s.parent().cleanWhitespace();s.children().cleanWhitespace();s.children().css("display","block");var x=g["inner"+v[0][f]+"item.orientation"];s.length>
0&&x&&x=="horizontal"?(x=u(s),s.css("width",x+"px"),n&&((n=="left"||n=="center")&&s.css("margin-right","auto"),(n=="right"||n=="center")&&s.css("margin-left","auto"))):(s.css("float","left"),s.children().css({"float":"left",clear:"left"}))}}for(var m in g)(f=m.match(/^content_(.*)\.opacity/i))&&k.find("."+f[1]).applyOpacity(g[m]);var H,O;g.beforeAniFunc=function(b){if(I.browser.msie)for(var d=0;d<v[0].length;d++)H=c("#"+j+"_"+v[0][d]+"_in ul").children("li"),O=c("#"+j+"_"+v[0][d]+" ul").children("li"),
H.add(O).each(function(){c(this).children().each(function(){c(this).is(".active")&&c(this).css("display")=="block"&&(c(this).css("display","none"),c(this).parent().children("a").not(".active, .hover").css("display","block"))})}),H.eq(b-1).children().not(".active").css("display","none"),H.eq(b-1).children().filter(".active").css("display","block"),O.eq(b-1).children().not(".active").css("display","none"),O.eq(b-1).children().filter(".active").css("display","block");else for(d=0;d<v[0].length;d++)O=
c("#"+j+"_"+v[0][d]+" ul").children("li"),O.children().removeClass("active"),O.eq(b-1).children().addClass("active"),H=c("#"+j+"_"+v[0][d]+"_in ul").children("li"),H.children().removeClass("active"),H.eq(b-1).children().addClass("active")};g.afterAniFunc=function(c){g.loop==!1&&y&&y.getValue("autoAnimation")==!0&&c==y.getValue("totalSlides")&&(b(),y.stopAuto())};I.browser.msie&&(c("div[id^='"+j+"_numbers']"),c("div[id^='"+j+"_bullets'] ul, div[id^='"+j+"_numbers'] ul, div[id^='"+j+"_navbar'] ul").children("li").each(function(){var b=
c(this).children();c(this).parent().parent().is(c("div[id^='"+j+"_bullets']"))&&b.css({fontSize:"0px",lineHeight:"0px"});b.css("z-index","1");b.css("position","relative");var d=b.clone();d.addClass("active");c(this).append(d);d.css("display","none");var e=b.clone();e.addClass("hover");c(this).append(e);e.css("display","none");b.addClass("normal");b.css("display","block");if(c(this).parent().parent().is(c("div[id^='"+j+"_bullets'], div[id^='"+j+"_numbers']"))){var f=d.getBgImage(g.gifPath,d);f&&(d.append(f),
d.css("background-image","none"));if(d=e.getBgImage(g.gifPath,e))e.append(d),e.css("background-image","none");if(e=b.getBgImage(g.gifPath,b))b.append(e),b.css("background-image","none")}}));f=k.outerWidth(!0);m=k.outerHeight(!0);var B=c("<div/>");B.attr("id",""+j+"ContainerJS");B.css("width",f);B.css("height",m);B.css("overflow","hidden");B.css("position",k.css("position"));B.css("top",k.css("top"));B.css("left",k.css("left"));B.css("bottom",k.css("bottom"));B.css("right",k.css("right"));(g.align==
"left"||g.align=="center")&&B.css("margin-right","auto");(g.align=="right"||g.align=="center")&&B.css("margin-left","auto");k.wrap(B).css("overflow","visible");var B=c("#"+j+"ContainerJS"),y=c(this).sudoSlider(g),V=parseInt(k.css("margin-left")),Q=parseInt(k.css("margin-right")),L=parseInt(k.css("margin-bottom")),W=parseInt(k.css("margin-top")),V=isNaN(V)?0:V,Q=isNaN(Q)?0:Q,L=isNaN(L)?0:L,W=isNaN(W)?0:W,K=parseInt(k.css("border-left-width")),M=parseInt(k.css("border-right-width")),N=parseInt(k.css("border-bottom-width")),
F=parseInt(k.css("border-top-width")),K=isNaN(K)?0:K,M=isNaN(M)?0:M,N=isNaN(N)?0:N,F=isNaN(F)?0:F,f=parseInt(k.css("padding-left"));m=parseInt(k.css("padding-right"));n=parseInt(k.css("padding-bottom"));s=parseInt(k.css("padding-top"));f=isNaN(f)?0:f;m=isNaN(m)?0:m;var n=isNaN(n)?0:n,s=isNaN(s)?0:s,X,U=0,s=k.css("padding-left")?parseInt(k.css("padding-left").replace("px","")):0,x=k.css("padding-right")?parseInt(k.css("padding-right").replace("px","")):0;X=k.css("padding-top")?parseInt(k.css("padding-top").replace("px",
"")):0;U=k.css("padding-bottom")?parseInt(k.css("padding-bottom").replace("px","")):0;c.browser.msie&&parseInt(c.browser.version,10)<7&&(f=c("<div/>"),f.css({top:"0px",left:"0px",position:"absolute"}),k.prepend(f),f.css("background-image",k.css("background-image")),f.css("width",k.outerWidth()),f.css("height",k.outerHeight()),k.css("background-image","none"));for(f=0;f<D.length;f++)for(n=0;n<v[f].length;n++){m=c("#"+j+"_"+v[f][n]+"_in");m.css("z-index",99999);var G=m.css("left")?parseInt(m.css("left").replace("px",
"")):0,Z=m.css("right")?parseInt(m.css("right").replace("px","")):0,aa=m.css("top")?parseInt(m.css("top").replace("px","")):0,w=m.css("bottom")?parseInt(m.css("bottom").replace("px","")):0;m.css("left")!="auto"&&m.css("left",G+s+"px");m.css("right")!="auto"&&m.css("right",Z+x+"px");m.css("top")!="auto"&&m.css("top",aa+X+"px");m.css("bottom")!="auto"&&m.css("bottom",w+U+"px");c("#"+j+"").parent().append(m)}f=c("div[id^='"+j+"_back']");m=c("div[id^='"+j+"_next']");n=c("#"+j+"_playpause_in").add(c("#"+
j+"_playpause")).children().filter(".play");s=c("#"+j+"_playpause_in").add(c("#"+j+"_playpause")).children().filter(".pause");I.browser.msie&&(n.attr("id",j+"_playpause_in_play"),s.attr("id",j+"_playpause_in_pause"),n=c("#"+j+"_playpause_in_play").add("#"+j+"_playpause_play"),s=c("#"+j+"_playpause_in_pause").add("#"+j+"_playpause_pause"),x=function(b,d){b.each(function(){var b=c('<div id="'+c(this).attr("id")+'_wrapper"></div>');b.attr("class",c(this).attr("class"));var f=c(this).clone(),g=c(this).clone();
f.addClass("normal");f.attr("style","");f.css("position","static");f.css("display","block");f.appendTo(b);g.addClass("hover");g.attr("style","");g.css("position","static");g.css("display","none");g.attr("id",g.attr("id"));g.appendTo(b);b.css("top",c(this).css("top"));b.css("bottom",c(this).css("bottom"));b.css("left",c(this).css("left"));b.css("right",c(this).css("right"));b.css("position","absolute");b.css("width",c(this).css("width"));b.css("height",c(this).css("height"));c(this).replaceWith(b);
d.push(b)})},X=[],U=[],G=[],Z=[],x(f,X),x(m,U),x(n,G),x(s,Z),f=c(X[0]),m=c(U[0]),n=c(G[0]),s=c(Z[0]),n.css("position","static"),s.css("position","static"),f.css("z-index",1E5),m.css("z-index",1E5),n.css("z-index",1E5));c("div[id^='"+j+"_playpause_in']").css("z-index",1E5);c(f).add(m).add(n).add(s).hover(function(){var b=c(this);I.browser.msie?(b.children().filter(".hover").css("display","block"),b.children().filter(".normal").css("display","none")):b.addClass("hover")}).mouseleave(function(){var b=
c(this);I.browser.msie?(b.children().filter(".hover").css("display","none"),b.children().filter(".normal").css("display","block")):b.removeClass("hover")});n=c("div[id^='"+j+"_numbers']").css("background-image");s=c("div[id^='"+j+"_navbar']").css("background-image");x=c("div[id^='"+j+"_bullets']").css("background-image");k.children().fixPng(g.gifPath,!1);k.children().children().filter("ul").fixPng(g.gifPath,!0);c("div[id^='"+j+"_numbers']").css("filter","");c("div[id^='"+j+"_numbers']").css("background-image",
n);c("div[id^='"+j+"_navbar']").css("filter","");c("div[id^='"+j+"_navbar']").css("background-image",s);c("div[id^='"+j+"_bullets']").css("filter","");c("div[id^='"+j+"_bullets']").css("background-image",x);f.click(function(c){c.preventDefault();g.stopAtInteraction&&(y.stopAuto(),b());y.goToSlide("prev")});m.click(function(c){c.preventDefault();g.stopAtInteraction&&(y.stopAuto(),b());y.goToSlide("next")});for(f=0;f<D.length;f++)for(n=0;n<v[f].length;n++)m=c("#"+j+"_"+v[f][n]+"_in_wrapper"),m.length||
(m=c("#"+j+"_"+v[f][n]+"_in")),s=Y[f]?"inner"+Y[f][n]:D[f],x=1,g[s+".opacity"]&&(x=parseFloat(g[s+".opacity"])),g[s+".hideOnMouseOut"]&&m.length&&l(m,s,x);var t=c("#"+j+"_playpause_in");t.click(function(f){f.preventDefault();c(this).children().filter(".play").css("display")=="block"?(y.getValue("autoAnimation")==!1&&y.startAuto(),d()):(y.stopAuto(),b())});g.auto==!0?d():(y.stopAuto(),b());for(f=0;f<v[0].length;f++)if((D=c("div[id='"+j+"_"+v[0][f]+"_in'] ul li"))&&D.length!=0)D.children().css("float",
"left"),D.children().css("clear","left"),D.hover(function(){I.browser.msie?c(this).children().filter(".active").css("display")!="block"&&(c(this).children().not(".hover").css("display","none"),c(this).children().filter(".hover").css("display","block")):c(this).children().addClass("hover")},function(){if(I.browser.msie){var b;c(this).parent().children().children().filter(".active").each(function(){c(this).css("display")=="block"&&(b=c(this).parent())});c(this).children("a").filter(".hover").css("display",
"none");!b||b.is(this)?c(this).children("a").filter(".active").css("display","block"):c(this).children("a").not(".hover, .active").css("display","block")}else c(this).children("a").removeClass("hover")});c("div[id^='"+j+"_numbers'] ul, div[id^='"+j+"_bullets'] ul, div[id^='"+j+"_navbar'] ul").children("li").click(function(d){d.preventDefault();g.stopAtInteraction&&(y.stopAuto(),b());y.goToSlide(c(this).index()+1)});return y}})(xtd_jQuery);